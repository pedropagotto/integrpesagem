﻿
/*Desenvolvido por: Pedro Pagotto
 Data: 20-04-2017
 Classe para realizar a leitura da DLL da balança toledo
 na unidade de SALTO.
 */

using IntegrPesagem.Objects;
using System;
using System.Runtime.InteropServices;
namespace IntegrPesagem.Reader
{
    public class ReaderBalance
    {
        //Bloco de importação dos metodos contidos dentro da DLL da Toledo

        //Fecha o canal de comunicação entre a dll e a balança
        [DllImport("PClink6.dll")]
        private static extern void Close_Canal(int Canal);

        //Fecha o canal de comunicação entre a dll e a balança
        [DllImport("PClink6.dll")]
        private static extern void Deleta_Canal(int Canal);

        //Realiza a leitura do peso e grava na memoria interna da balança
        [DllImport("PClink6.dll")]
        private static extern int Update_Canal(int Canal);

        //Realiza a leitura do peso gravado pelo update_canal e converte para peso bruto
        [DllImport("PClink6.dll")]
        private static extern int Gross_Canal(int Canal);
        
        //Metodo responsavel por verificar qual porta esta disponivel para a balança
        [DllImport("PClink6.dll")]
        private static extern int W9091Serial(int Canal);
        
        public Weighing ReaderWeight(int porta)
        {
            Weighing w = new Weighing();
            try
            {
                /*Realiza a leitura do canal da balança na dll da toledo :*/
                w.canal = W9091Serial(porta);

                /*Realiza a leitura da balança e grava internamente na memoria 
                 * Tipos de retornos possiveis:
                 *Retorno : 
                 * 0 - Indica que a leitura foi válida
                 * 1 - Indica que não foi possível efetuar a leitura da balança.
                 * 3 - Indica que há sobrecarga sobre a balança. Exemplo: na plataforma de um indicador 9091, é colocado 26 kg , que tem uma carga máxima de 25kg.O display apaga , mas o indicador continua transmitindo.Isto deve ser considerado como erro.
                 *999 - Indica que não conseguiu detectar a hardkey local ou de rede.
                 */
                w.updateWeigth = Update_Canal(w.canal);

                switch (w.updateWeigth)
                {
                    case 0 :
                        //Realiza a extração do peso bruto registrado pelo metodo update_canal
                        w.grossExctract = Gross_Canal(w.canal);
                        //Realiza a conversão dos dados retornados em Pchar para string
                        w.grossWeigth = Marshal.PtrToStringAnsi((IntPtr)w.grossExctract);
                        //Remove o espaço que tem dentro da string
                        w.grossWeigth = w.grossWeigth.Trim();
                        //seta uma string vazia para que o prog possa gravar o txt sem erros.
                        w.msg = "";
                        break;
                    //Erros tratados para exibição do usuario via janela de pop up.
                    case 1 :
                        w.msg = "não foi possível efetuar a leitura da balança";
                        break;
                    case 3:
                        w.msg = "Indica que há sobrecarga sobre a balança. Exemplo : na plataforma de um indicador 9091, é colocado 26 kg , que tem uma carga máxima de 25kg";
                        break;
                    case 999:
                        w.msg = "Programa de pesagem não conseguiu detectar a hardkey local ou de rede.";
                        break;
                }

                //Realiza a finalização do canal de integração entre a dll e a balança
                Close_Canal(w.canal);
                Deleta_Canal(w.canal);
                
            }
            catch (Exception e)
            {
                w.msg = e.Message;
            }

            return w;           
        }
    }
}