﻿/*Desenvolvido por: Pedro Pagotto
 Data: 20-04-2017
 Classe para realizar a escrita de exceptions e do peso coletado da balança em arquivos .txt
 */
using System;
using System.IO;
using IntegrPesagem.Constants;

namespace IntegrPesagem.Writer
{
    public class ActionsWriters
    {
        public void WriteTxt(string text, string log)
        {
            /*Diretorios necessarios para funcionamento da aplicaçao*/
            string folder = @"C:\YpeBalancaToledoTI420";
            string FileWeight = @"C:\YpeBalancaToledoTI420\_.txt";
            string folderLog = @"C:\YpeBalancaToledoTI420\Log";


            /*Cria todoas as pastas para pesagem*/
            if (!Directory.Exists(folder) || !Directory.Exists(folderLog))
            {
                Directory.CreateDirectory(folder);
                Directory.CreateDirectory(folderLog);
            }

            /*Realiza a gravação do txt com o peso enviado pelo metodo de leitura do peso da balança
             * é registrado dentro do diretorio padrão da aplicação : C:\YpeBalancaToledoTI420
             * o nome do arquivo sempre será _.txt
             */
            if (log == ConstantsConfig.SUCESS)
            {
                //Metodo que realiza a escrita do txt no diretorio
                using (StreamWriter sw = new StreamWriter(FileWeight))
                {
                        //Realiza a escrita do txt e logo apos fecha o arquivo.
                        sw.Write(text);
                        sw.Close();
                }
            }
            else
            {
                /*realiza a gravação do arquivo de log no diretorio de log com erro*/
                //Coleta data para registro do log
                var dateLog = DateTime.Now.ToString().Replace('/','.');
                var hour = DateTime.Now.TimeOfDay.ToString().Substring(0,8);
                dateLog = dateLog.Substring(0, 10);

                //Nomeando o arquivo com a data e o nome de ErrorLog
                string fileLog = folderLog + @"\ErrorLog-" + dateLog + ".txt";

                //Metodo que realiza a leitura do erro enviado pelo metodo de leitura de peso da balança
                using (StreamWriter sw = new StreamWriter(fileLog,true))
                {
                     
                    if (File.Exists(fileLog))
                    {
                        sw.WriteLine(hour + " - : " + text);
                    }
                    else
                    {
                        sw.Write(hour+" - : "+text);
                    }
                }
            }
        }
    }
}
