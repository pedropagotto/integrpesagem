﻿using IntegrPesagem.Constants;
using IntegrPesagem.Objects;
using IntegrPesagem.Reader;
using IntegrPesagem.Writer;
using System;

namespace IntegrPesagem
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciando objetos para trabalho
            ActionsWriters aw = new ActionsWriters();
            ReaderBalance rb = new ReaderBalance();
            Weighing wei = new Weighing();
            
           try
            {
                Console.WriteLine("Iniciando leitura de pesagem porta COM3");
                //Realiza a leitura da balança
                wei = rb.ReaderWeight(ConstantsConfig.COM);

                //Se o retorno for = "" a leitura do peso foi realizado com sucesso
                if (wei.msg == "")
                {
                    Console.WriteLine("Peso Retornado : " + wei.grossWeigth);
                    Console.WriteLine("Enviando para o SAP");
                    //Realiza a gravação do peso coletado no arquivo txt para o ABAP consumir.
                    aw.WriteTxt(wei.grossWeigth, ConstantsConfig.SUCESS);
                    Console.WriteLine("Leitura Finalizada");
                }else
                {
                    //Realiza registro de log com a msg de erro e mostra uma janela de erro para o usuario
                    aw.WriteTxt(wei.msg, ConstantsConfig.ERROR);
                    System.Windows.Forms.MessageBox.Show("Problemas ao ler o peso da balança, valor retornado: " + wei.msg + "peso : " + wei.grossWeigth, "Erro:", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }

            }
            catch (Exception e)
            {
                //Realiza registro de log com a msg de erro e mostra uma janela de erro para o usuario
                aw.WriteTxt(e.Message,ConstantsConfig.ERROR);
                System.Windows.Forms.MessageBox.Show("Problemas ao conectar na balança: "+e.Message,"Erro:",System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Error);
            }
            
        }
    }
}
