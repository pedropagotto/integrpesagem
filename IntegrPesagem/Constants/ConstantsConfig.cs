﻿/*Desenvolvido por: Pedro Pagotto
 Data: 20-04-2017
 Classe para declarar constants de configuração do programa
 na unidade de SALTO.
 */
namespace IntegrPesagem.Constants
{
    class ConstantsConfig
    {
        /*CONSTANTS DE LOGS*/
        public const string ERROR = "E";
        public const string SUCESS = "S";


        /*Constants para Chamar a balança*/
        public const int COM = 3;
    }
}
